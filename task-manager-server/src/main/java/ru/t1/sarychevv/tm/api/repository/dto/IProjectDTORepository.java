package ru.t1.sarychevv.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.dto.model.ProjectDTO;

public interface IProjectDTORepository extends IUserOwnedDTORepository<ProjectDTO> {

    @NotNull
    ProjectDTO create(@NotNull String userId,
                      @NotNull String name,
                      @NotNull String description) throws Exception;

    @NotNull
    ProjectDTO create(@NotNull String userId,
                      @NotNull String name) throws Exception;

}

